# Guía de contribución #

Si deseas aportar tus modificaciones al proyecto, es bastante sencillo, lo
primero que debes hacer es clonar este repositorio:

```bash
$ git clone git@gitlab.com:radiognu/radiognu-widget.git
```

Luego de hacer tus modificaciones en los archivos fuente, que están en `src`,
debes construir el resultado, para ello, debes instalar lo necesario usando
`npm`:

```bash
$ npm install
```

Finalmente, puedes construir con el siguiente comando:

```bash
$ npm run build
```

Si hay algún error, te indicará de inmediato para que lo puedas corregir. Una
vez hechos los cambios, puedes crear una solicitud de unión (Merge Request)
desde tu repositorio.

Si tienes errores al construir, aún así recomendamos hacer una solicitud,
pegando el mensaje de error como comentario.

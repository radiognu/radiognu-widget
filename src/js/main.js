/*
 * main.js -- Javascript para el widget del proyecto radiognu-widget
 *
 * @license Copyright 2013 - 2017 BreadMaker aka CTM <breadmaker@radiognu.org>
 *
 * Este programa es software libre; puede redistribuirlo y/o modificarlo bajo
 * los términos de la Licencia Pública General GNU tal como se publica por
 * la Free Software Foundation; ya sea la versión 3 de la Licencia, o
 * (a su elección) cualquier versión posterior.
 *
 * Este programa se distribuye con la esperanza de que le sea útil, pero SIN
 * NINGUNA GARANTÍA; sin incluso la garantía implícita de MERCANTILIDAD o
 * IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Vea la Licencia Pública
 * General de GNU para más detalles.
 *
 * Debería haber recibido una copia de la Licencia Pública General de GNU
 * junto con este programa; de lo contrario escriba a la Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, EE. UU.
 */

var titl;

function formatTitle() {
    if ($("#wid-listeners-quantity:not(:empty)").length !== 0 && !isNaN(parseInt($(
            "#wid-listeners").data("quantity")))) document.title = parseInt($(
        "#wid-listeners").data("quantity")) + " escuchando " + titl;
}

// Verifica si un selector existe en el DOM
jQuery.fn.exists = function () {
    return this.length > 0;
};

var manualFetchTimer;

function initSocket() {
    var socket = io.connect("https://radiognu.org:7777", {
        transports: ["websocket", "htmlfile", "xhr-polling", "jsonp-polling"]
    });
    var firstConnect = true;
    socket.on('connect', function () {
        console.debug("Se ha conectado exitosamente al socket.");
        if (manualFetchTimer !== undefined) {
            clearInterval(manualFetchTimer);
        }
    });
    socket.on('metadata', function (data) {
        if (!firstConnect)
            getInfo(data);
        firstConnect = false;
    });
    socket.on('listeners', function (listeners) {
        $("#wid-listeners-quantity").text(listeners);
        $("#wid-listeners").data({
            quantity: listeners
        });
    });
    socket.on('connect_error', function (data) {
        console.debug("Ha ocurrido un error con la conexión.");
        if (manualFetchTimer === undefined) {
            manualFetchTimer = setInterval(function () {
                getInfo();
            }, 30000);
        }
    });
    socket.on('disconnect', function () {
        console.debug("Se ha perdido la conexión con el socket.");
    });
    socket.on('reconnecting', function () {
        console.debug("Se ha iniciado la reconexión con el socket.");
    });
    socket.on('reconnect', function () {
        console.debug("La reconexión con el socket ha sido exitosa.");
        firstConnect = false;
    });
    socket.on('reconnect_failed', function () {
        console.debug("La reconexión con el socket ha fallado.");
    });
}

var artist, title, currentMetadata = {
    artist: "",
    title: ""
};

function getInfo(metadata) {
    if ((navigator.hasOwnProperty("onLine") || Navigator.prototype.hasOwnProperty(
            "onLine")) && navigator.onLine) {
        if (metadata !== undefined) {
            if (currentMetadata.artist !== metadata.artist || currentMetadata.title !==
                metadata.title) {
                console.debug("Se han recibidos nuevos metadatos desde el servidor",
                    metadata);
                currentMetadata = metadata;
                $("#wid-now-playing-loading").fadeIn(500);
                $.getJSON("https://api.radiognu.org/?no_cover", metadata, function (
                    data) {
                    $("#wid-now-playing-loading").fadeOut(500);
                    if (data.artist === null && data.title === null)
                        showIcecastError();
                    else processDataFromAPI(data);
                });
            } else {
                console.debug("Se han recibidos metadatos duplicados desde el servidor",
                    metadata);
            }
        } else {
            $("#wid-now-playing-loading").fadeIn(500);
            $.getJSON("https://api.radiognu.org/?no_cover", function (data) {
                $("#wid-now-playing-loading").fadeOut(500);
                if (artist != data.artist || title != data.title)
                    processDataFromAPI(data);
                else if (data.artist === null && data.title === null)
                    showIcecastError();
                else {
                    $("#wid-listeners-quantity").text(data.listeners);
                    $("#wid-listeners").data({
                        quantity: data.listeners
                    });
                }
            });
        }
    }
}

function processDataFromAPI(data) {
    if ($("#wid-loading-message").exists()) {
        isFirstTime = true;
        startWidget();
    }
    animImg("https://api.radiognu.org/cover/" + data.album_id + ".png?img_size=175");
    animDiv(".wid-now-playing-titulo", data.title);
    setTimeout(function () {
        animDiv(".wid-now-playing-artista", data.artist);
    }, 100);
    setTimeout(function () {
        animDiv(".wid-now-playing-album", data.album);
    }, 200);
    setTimeout(function () {
        animDiv(".wid-now-playing-other", {
            "country": data.country,
            "year": data.year,
            "license": data.license
        });
    }, 300);
    $("#wid-listeners-quantity").text(data.listeners);
    $("#wid-listeners").data({
        quantity: data.listeners
    });
    artist = data.artist;
    title = data.title;
    if (data.isLive) {
        $("#wid-now-playing-live-indicator").fadeIn(500);
    } else {
        $("#wid-now-playing-live-indicator").fadeOut(500);
    }
    if (isFirstTime)
        $("iframe", window.parent.document).height($("#radiognu-widget").height());
}

function animDiv(div, info) {
    var newDiv;
    if (div == ".wid-now-playing-other") {
        newDiv = $(div + " .wid-now-playing-info-line").clone().addClass("in");
        newDiv.find(".wid-now-playing-pais").html(info.country);
        newDiv.find(".wid-now-playing-año").html(info.year);
        if (info.license === "") {
            newDiv.find(".wid-now-playing-separator:last").addClass("hide");
            newDiv.find(".wid-now-playing-licencia").removeAttr("title").empty();
        } else {
            var license = info.license.shortname.split(" ");
            newDiv.find(".wid-now-playing-separator.hide").removeClass("hide");
            newDiv.find(".wid-now-playing-licencia").removeAttr("title").html($(
                "<a>").attr({
                href: info.license.url,
                target: "_blank",
                title: "Licencia: " + info.license.name
            }).html("<i class='icon-cc'></i> " + license[1] + " " +
                license[2] + (license.length === 4 ?
                    " <img src='https://cdn.rawgit.com/hjnilsson/country-flags/0489a1d2/svg/" +
                    (license[3].toLowerCase() === "uk" ? "gb" : license[
                        3].toLowerCase()) +
                    ".svg' onerror='this.onerror=null; this.src=\"https://cdn.rawgit.com/hjnilsson/country-flags/0489a1d2/png250px/" +
                    (license[3].toLowerCase() === "uk" ? "gb" : license[
                        3].toLowerCase()) + ".png\"' alt=''>" : "")));
        }
    } else {
        newDiv = $(div + " .wid-now-playing-info-line").clone().addClass("in").html(
            info);
    }
    $(div).append(newDiv).find(".wid-now-playing-info-line:first").addClass("out").emulateTransitionEnd(
        750).one("bsTransitionEnd", function () {
        $(this).remove();
    });
    setTimeout(function () {
        $(div + " .wid-now-playing-info-line.in").removeClass("in");
    }, 17); // 1000 / 60 (fps) = 16.666666666...
}

function showIcecastError() {
    if ($("#wid-loading-message").exists()) {
        $("#wid-loading-message-text").html($("<i class='icon-frown'></i>")).append(
            " Lo sentimos, no hay conexión al servidor Icecast. Inténtelo más tarde."
        );
    }
}

function errorAlbumImg() {
    if ($(".wid-np-album-img:first img").attr("src") !== "") {
        $(".wid-np-album-img:first").animate({
            "opacity": ".5"
        }, {
            duration: 500,
            queue: false
        }).css("cursor", "pointer").children().animate({
            "opacity": "1"
        }, {
            duration: 500,
            queue: false
        }).parent().off("click").click(function () {
            var s = $(".wid-np-album-img img:first").attr("src");
            $(".wid-np-album-img img:first").attr("src", "").attr("src", s);
        });
    }
}

function animImg(src) {
    if (src === undefined) src = "https://api.radiognu.org/cover/0.png?img_size=175";
    var newCover = $(".wid-now-playing-image:first").clone().addClass("in").children(
        "img").removeAttr("src").attr({
        "src": src,
        "onerror": "errorAlbumImg()"
    }).parent();
    $(".wid-now-playing-image:first").after(newCover).addClass("out").emulateTransitionEnd(
        750).one("bsTransitionEnd", function () {
        $(this).remove();
    });
    setTimeout(function () {
        $(".wid-now-playing-image.in").removeClass("in");
    }, 17); // 1000 / 60 (fps) = 16.666666666...
}

var utcTime = false;

function initClock() {
    $("#wid-time-clock").flipcountdown({
        speedFlip: 30,
        size: (window.innerWidth < 822) ? "sm" : "md",
        tick: function () {
            date = new Date();
            return new Date(date.getUTCFullYear(), date.getUTCMonth(), date
                .getUTCDate(), date.getUTCHours(), date.getUTCMinutes(),
                date.getUTCSeconds(), date.getUTCMilliseconds());
        },
        showSecond: false
    }).click(function () {
        utcTime = !utcTime;
        setUTCToLocalClock((window.innerWidth < 822) ? true : undefined);
    });
    $(".xdsoft_digit.xdsoft_separator").addClass("fade");
    setInterval(function () {
        $(".xdsoft_digit.xdsoft_separator").toggleClass("in");
    }, 1000);
}

function checkForUTCToLocalClockSize() {
    if (window.innerWidth < 822) {
        if ($("#wid-time-clock .xdsoft_flipcountdown").hasClass("xdsoft_size_md"))
            setUTCToLocalClock(true);
    } else {
        if ($("#wid-time-clock .xdsoft_flipcountdown").hasClass("xdsoft_size_sm"))
            setUTCToLocalClock();
    }
}

function setUTCToLocalClock(smallClock) {
    var date = new Date();
    if (($("#wid-time-clock .xdsoft_flipcountdown").hasClass("xdsoft_size_md") &&
            smallClock) || ($("#wid-time-clock .xdsoft_flipcountdown").hasClass(
            "xdsoft_size_sm") && smallClock === undefined)) {
        $("#wid-time-clock").empty().removeData();
    }
    if (utcTime) {
        $("#wid-time-clock").flipcountdown({
            speedFlip: 30,
            size: smallClock ? "sm" : "md",
            tick: function () {
                return date;
            },
            showSecond: false
        }).attr("title", "Clic para ver la hora UTC");
        var tz = String(String(date).split("(")[1]).split(")")[0];
        // Si no hay nombre de zona horaria definida por el sistema, entonces
        // se devuelve un nombre de zona horaria genérico
        tz = (tz === undefined) ? "Hora local" : tz;
        $("#wid-time-zone").html($("<span/>").attr({
            class: "wid-clock-timezone-name",
            title: tz
        }).html(tz));
    } else {
        $("#wid-time-clock").flipcountdown({
            speedFlip: 30,
            size: smallClock ? "sm" : "md",
            tick: function () {
                return new Date(date.getUTCFullYear(), date.getUTCMonth(),
                    date.getUTCDate(), date.getUTCHours(), date.getUTCMinutes(),
                    date.getUTCSeconds(), date.getUTCMilliseconds());
            },
            showSecond: false
        }).attr("title", "Clic para ver la hora local");
        $("#wid-time-zone").html($("<a/>").attr({
            href: "https://es.wikipedia.org/wiki/Tiempo_universal_coordinado",
            class: "wid-clock-timezone-name",
            title: "Tiempo Universal Coordinado",
            target: "_blank"
        }).text("UTC"));
    }
}

function initWidget() {
    $("#wid-now-playing-live-indicator").fadeOut(0);
    $("#wid-about-button").hover(function () {
        $(this).addClass("btn-info").removeClass("btn-link");
    }, function () {
        $(this).removeClass("btn-info").addClass("btn-link");
    }).click(function () {
        $("#wid-sidebar-info").collapse("hide");
        $("#wid-sidebar-about").collapse("show");
    });
    $("#wid-sidebar-about-close").click(function () {
        $("#wid-sidebar-info").collapse("show");
        $("#wid-sidebar-about").collapse("hide");
    });
    new UniversalHTML5Audio({
        selector: "radiognu",
        source_message: "Selecciona señal",
        className: "navbar navbar-default container-fluid",
        sources: [{
            name: 'Normal',
            icon: 'icon-signal-4',
            url: 'https://audio.radiognu.org/radiognu.ogg'
        }, {
            name: 'Liviana',
            icon: 'icon-signal-3',
            url: 'https://audio.radiognu.org/radiognu2.ogg'
        }, {
            name: 'LoFi',
            icon: 'icon-signal-2',
            url: 'https://audio.radiognu.org/radiognu3.ogg'
        }, {
            name: 'AM',
            icon: 'icon-signal-1',
            url: 'https://audio.radiognu.org/radiognuam.ogg'
        }]
    });
}

function startWidget() {
    $("#wid-loading-message-text").html("<i class='icon-ok'></i> ¡Conectado!");
    $("#wid-now-playing-info").removeClass("hide");
    $("#wid-loading-message").animate({
        "opacity": "0"
    }, 1000, function () {
        $(this).css("display", "none").remove();
        $("iframe", window.parent.document).height($("#radiognu-widget").height());
    });
}

$(document).ready(function () {
    titl = document.title;
    initWidget();
    initSocket();
    getInfo();
    initClock();
    formatTitle();
    setInterval(formatTitle, 1000);
    // Verificando si el usuario está usando la combinación que nos odia
    if (navigator.userAgent.indexOf("Android") >= 0 && navigator.userAgent.indexOf(
            "Chrome") >= 0) {
        $("#chrome-in-android-bad-combo").modal("show");
    } else {
        // En caso contrario, no necesitamos el modal
        $("#chrome-in-android-bad-combo").remove();
    }
});

var updateTimeout = false,
    scheduleUpdate = function (delay) {
        clearTimeout(updateTimeout);
        updateTimeout = setTimeout(function () {
            try {
                window.applicationCache.update();
            } catch (ex) {
                console.log('appCache.update: ' + ex);
            }
        }, delay || 300000);
    };

scheduleUpdate(3000);

$(window).load(function () {
    // Verifica si hay un nuevo appCache disponible
    window.applicationCache.addEventListener('updateready', function (e) {
        if (window.applicationCache.status == window.applicationCache.UPDATEREADY) {
            $("#chrome-in-android-bad-combo").modal("hide");
            $("#new-version-available").modal("show");
            scheduleUpdate();
        }
    }, false);
    window.applicationCache.addEventListener('noupdate', function () {
        scheduleUpdate();
    }, false);
    window.applicationCache.addEventListener('error', function () {
        scheduleUpdate();
    }, false);
    formatTitle();
    $("iframe", window.parent.document).height($("#radiognu-widget").height());
    // $(".wid-title").hoverForMore({
    //     speed: 50.0,
    //     removeTitle: false
    // });
}).resize(function () {
    checkForUTCToLocalClockSize();
    $("iframe", window.parent.document).height($("#radiognu-widget").height());
});
